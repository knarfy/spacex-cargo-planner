import React, {useContext, useEffect, useState} from 'react';
import styled from 'styled-components';
import axios from 'axios';
import {ShipmentContext} from '../context/ShipmentsContext';
import Button from '../components/Button';
import Input from '../components/Input';

export default function Shipment({shipment}) {
	const context = useContext(ShipmentContext);
	const [state, setState] = useState({error: false, noLocalData: false});
	const reg = RegExp('^([0-9]?[.]?[0-9]?[,]?)([0-9,. ]+)$');

	useEffect(checkIfAvailableLocally, [shipment]);

	function checkIfAvailableLocally() {
		if (context.appState.fetchType === 'local') {
			const shipments = JSON.parse(localStorage.getItem('shipments')) ?? [];
			const index = shipments.findIndex(item => item.id === shipment.id);

			if (index < 0) {
				setState({noLocalData: true});
			} else {
				setToActiveRecord(shipments[index]);
			}
		} else {
			setToActiveRecord(shipment);
		}
	}

	function setToActiveRecord(data) {
		setState({noLocalData: false});
		context.setActiveRecord(data);
	}

	function calculateCargoBays(boxes) {
		if (typeof boxes === 'undefined' || boxes === null) return 0;

		const value = boxes?.split(',').reduce((totalUnits, unit) => {
			// Convert string values to numbers then add them

			if (isNaN(+unit)) {
				return +totalUnits;
			} else {
				return +totalUnits + +unit;
			}
		});

		// Return the highest whole number
		return Math.ceil(value / 10);
	}

	function onChangeCargo(event) {
		// TODO add a debounce function to avoid costly queries while user is still typing within 100ms delay
		// Regular expression check for invalid characters
		if (reg.test(event?.target?.value)) {
			context.setActiveRecord({
				...context.activeRecord,
				boxes: event?.target?.value
			});
			setState({error: false});
		} else {
			context.setActiveRecord({
				...context.activeRecord,
				boxes: null
			});
			setState({error: true});
		}
	}

	return (
		<Div>
			{state.noLocalData ? (
				<div className={'empty-message'}>
					<h2>This shipment doesn't exist in local. Load the one from network?</h2>
					<Button text={'Load'} onClick={() => setToActiveRecord(shipment)}/>
				</div>
			) : (
				<div className={'shipment-details'}>
					<h1>{context.activeRecord?.name}</h1>
					<a href={`mailto:${context.activeRecord?.email}`}>{context.activeRecord?.email}</a>
					<div>
						<span>Number of required cargo bays <strong>{calculateCargoBays(context.activeRecord?.boxes)}</strong></span>
					</div>
					<Input
						label={'Cargo boxes'}
						placeholder={'Cargo boxes'}
						error={state?.error ? 'Only numbers, comma, and period is allowed' : null}
						defaultValue={context.activeRecord?.boxes}
						onChange={onChangeCargo}
					/>
				</div>
			)}
		</Div>
	);
}

// NextJS Server-side fetch from [shipment_id] in the url
export async function getServerSideProps(context) {
	const response = await axios.get(`http://localhost:3000/api/shipments/${context.query.shipment_id}`);
	const shipment = await response.data;

	return {
		props: {
			shipment
		},
	};
}

const Div = styled.div`
	height: 100%;
	
	.empty-message {
		width: 30%;
		height: 100px;
		margin: 100px auto;
	}
	
	.shipment-details {
		max-width: 400px;
		margin: 16px auto 16px 16px;
	}
	
	.shipment-details > * {
		margin: 16px auto 16px 16px;
	}
	
	button {
		width: 100%;
		margin: 16px 0px;
	}
`;

