import React from 'react';
import * as PropTypes from 'prop-types';
import styled from 'styled-components';
import * as Colors from '../constants/Colors';

Button.propTypes = {
	text: PropTypes.string.isRequired,
	onClick: PropTypes.func,
};

export default function Button(props) {
	return (
		<StyledButton {...props}>
			<span className={'button-text'}>{props.text}</span>
		</StyledButton>
	);
}

const StyledButton = styled.button`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	height: 32px;
	background: ${Colors.black};
	border-radius: 8px;
	padding: 0px 16px;
	margin: 16px;

	:focus {
		outline: none;
	}

	:hover {
		cursor: pointer;
		opacity: 0.8;
	}

	.button-text {
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		line-height: 19px;
		text-align: center;
		color: ${Colors.white};
		user-select: none;
	}
`;
