import React, {createContext, useState} from 'react';
import axios from 'axios';

export const ShipmentContext = createContext(null);

// Global state will be manipulated here
export const ShipmentProvider = props => {
	const [shipments, setShipments] = useState([]);
	const [loadedShipments, setLoadedShipments] = useState([]);
	const [activeRecord, setActiveRecord] = useState({});
	const [appState, setAppState] = useState({fetchType: 'local'});

	function getLocallySavedShipments() {
		const shipments = JSON.parse(localStorage.getItem('shipments'));

		if (shipments) {
			setShipments(shipments);
			setLoadedShipments(shipments);
			setAppState({fetchType: 'local'});
		}
	}

	async function getShipmentsFromNetwork() {
		const response = await axios.get('http://localhost:3000/api/shipments');
		const shipments = await response.data;

		if (shipments) {
			setShipments(shipments);
			setLoadedShipments(shipments);
			setAppState({fetchType: 'network'});
		}
	}

	function saveShipmentsToLocal() {
		const shipments = JSON.parse(localStorage.getItem('shipments')) ?? [];

		const index = shipments.findIndex(item => item.id === activeRecord.id);

		if (index < 0) {
			shipments.push(activeRecord);
		} else {
			shipments[index] = activeRecord;
		}

		localStorage.setItem('shipments', JSON.stringify(shipments));

		getLocallySavedShipments();
	}

	return (
		<ShipmentContext.Provider
			value={{
				shipments,
				setShipments,
				loadedShipments,
				setLoadedShipments,
				activeRecord,
				setActiveRecord,
				appState,
				setAppState,
				getLocallySavedShipments,
				getShipmentsFromNetwork,
				saveShipmentsToLocal,
			}}
		>
			{props.children}
		</ShipmentContext.Provider>
	);
};
