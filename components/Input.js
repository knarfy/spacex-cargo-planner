import React from 'react';
import * as PropTypes from 'prop-types';
import styled from 'styled-components';
import * as Colors from '../constants/Colors';

Input.propTypes = {
	type: PropTypes.string,
	label: PropTypes.string,
	placeholder: PropTypes.string,
	defaultValue: PropTypes.node,
	onChange: PropTypes.func,
	error: PropTypes.node,
};

Input.defaultProps = {
	type: 'text',
	error: false,
};

export default function Input(props) {
	return (
		<StyledInput {...props}>
			{props.label && <span className={'label'}>{props.label}</span>}
			<input
				className={'input'}
				type={props.type}
				placeholder={props.placeholder}
				defaultValue={props.defaultValue}
				onChange={props.onChange}
			/>
			{props.error && <span className={'label-error'}>{props.error}</span>}
		</StyledInput>
	);
}

const StyledInput = styled.div`
	width: 100%;
	width: fill-available;
	margin: 16px;
		
	.label,
	.label-error {
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
		line-height: 19px;
		color: ${props => (props.error ? Colors.danger : Colors.black)};
	}

	.input {
		width: inherit;
		height: 32px;
		padding: 0px 16px;
		border: 1px solid ${props => (props.error ? Colors.danger : Colors.primary)};
		border-radius: 8px;
		box-shadow: 0px 4px 15px
			${props => (props.error ? Colors.box_shadow_danger : Colors.box_shadow_primary)};
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		line-height: 19px;
		color: ${Colors.black};

		::placeholder {
			color: rgba(0, 0, 0, 0.33);
		}

		:focus {
			outline: none;
		}
	}
`;
