// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import shipments from '../../../shipments.json';

export default (req, res) => {
	res.statusCode = 200;
	res.json(shipments);
}
