import React, {useContext, useEffect} from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import _ from 'lodash';
import {ShipmentContext} from '../context/ShipmentsContext';
import Button from './Button';
import Input from './Input';

export default function Layout({children}) {
	const context = useContext(ShipmentContext);
	useEffect(context.getLocallySavedShipments, []);

	function onSearch(event) {
		// TODO add a debounce function to avoid costly queries while user is still typing within 100ms delay
		const filteredShipments = _.filter(context.loadedShipments, function (item) {
			return item.name.toUpperCase().includes(event?.target?.value.toUpperCase());
		});

		context.setShipments(filteredShipments);
	}

	function renderShipment(item) {
		return (
			<div key={item?.id} className={'list-item'}>
				<Link href={`/[shipment_id]`} as={`/${item?.id}`}>
					<a><h2>{item?.name}</h2></a>
				</Link>
			</div>
		);
	}

	return (
		<Div>
			<div className={'header'}>
				<Link href={`/`}>
					<a><h1>Cargo Planner</h1></a>
				</Link>
				<Input placeholder={'Search'} onChange={onSearch}/>
				<Button text={'Load'} onClick={context.getShipmentsFromNetwork}/>
				<Button text={'Save'} onClick={context.saveShipmentsToLocal}/>
			</div>
			<div className={'content'}>
				<div className={'shipments-list'}>
					{context.shipments.length !== 0 ? context.shipments?.map(item => renderShipment(item)) : (
						<div className={'list-item'}>
							<h2>No shipments saved locally.</h2>
						</div>
					)}
				</div>
				<div className={'shipment'}>
					{children}
				</div>
			</div>
		</Div>
	);
}

const Div = styled.div`
	width: 100vw;
	height: 100vh;
	
	a {
		font-style: normal;
		font-weight: bold;
		font-size: 16px;
	}
	
	.header {
		position: fixed;
		top: 0px;
		width: 100%;
		width: fill-available;
		height: 75px;
		padding: 0px 16px;
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
		background: #CECECE;
	}
	
	.header > a, .header h1 {
		width: 300px;
		text-decoration: none;
	}
	
	.header {
		width: 100%;
		width: fill-available;
	}
	
	.content {
		display: flex;
		flex-direction: row;
		width: inherit;
		height: inherit;
	}
	
	.shipments-list {
		width: 300px;
		margin-top: 75px;
		overflow-y: scroll;
	}
	
	.shipments-list::-webkit-scrollbar {
	  width: 1em;
	}
	 
	.shipments-list::-webkit-scrollbar-track {
	  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
	}
	 
	.shipments-list::-webkit-scrollbar-thumb {
	  background-color: darkgrey;
	  outline: 1px solid slategrey;
	}
	
	.list-item {
		text-align: center;
	}
	
	.shipment {
		flex: 1;
		margin-top: 75px;
	}
`;
