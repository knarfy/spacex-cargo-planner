import React from 'react';
import '../styles/globals.css';
import Layout from '../components/layout';
import {ShipmentProvider} from '../context/ShipmentsContext';

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({Component, pageProps}) {
	return (
		<ShipmentProvider>
			<Layout>
				<Component {...pageProps} />
			</Layout>
		</ShipmentProvider>
	);
}
