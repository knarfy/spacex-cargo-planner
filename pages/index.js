import React, {useContext} from 'react';
import styled from 'styled-components';
import {ShipmentContext} from '../context/ShipmentsContext';
import Button from '../components/Button';

export default function Home() {
	const context = useContext(ShipmentContext);

	return (
		<Div>
			{context.shipments?.length === 0 && (
				<div className={'empty-message'}>
					<h2>There are no shipments saved locally. Please load over the network.</h2>
					<Button text={'Load'} onClick={context.getShipmentsFromNetwork}/>
				</div>
			)}
		</Div>
	);
}

const Div = styled.div`
	height: 100%;
	
	.empty-message {
		width: 30%;
		height: 100px;
		margin: 100px auto;
	}
	
	button {
		width: 100%;
		margin: 16px 0px;
	}
`;
