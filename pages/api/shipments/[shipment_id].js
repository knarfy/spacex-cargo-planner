// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import _ from 'lodash';
import shipments from '../../../shipments.json';

export default (req, res) => {
	let response = _.find(shipments, function (item) {
		return item.id === req.query.shipment_id;
	});

	res.statusCode = 200;
	res.json(response);
}
